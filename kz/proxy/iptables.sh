#!/bin/sh

iptables="/sbin/iptables"

# Flush old rules, old custom tables
iptables --flush
iptables --delete-chain

# Set default policies for all three default chains
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT ACCEPT

# Enable free use of loopback interfaces
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# All TCP sessions should begin with SYN
iptables -A INPUT -p tcp ! --syn -m state --state NEW -s 0.0.0.0/0 -j DROP

# Allow established, related packets
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# drop invalid syn packets
iptables -A INPUT -i eth0 -p tcp --tcp-flags ALL ACK,RST,SYN,FIN -j DROP
iptables -A INPUT -i eth0 -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
iptables -A INPUT -i eth0 -p tcp --tcp-flags SYN,RST SYN,RST -j DROP

# Allow certain subnets
iptables -A INPUT -s 173.248.144.240/28 -j ACCEPT
#iptables -A INPUT -s 50.134.165.166/32 -j ACCEPT

# Allow SSH
iptables -A INPUT -p tcp --dport 22 -m state --state NEW -s 50.134.165.166/32 -j ACCEPT

# Allow OpenSIPS packets
iptables -A INPUT -p tcp --dport 5060 -j ACCEPT
iptables -A INPUT -p udp --dport 5060 -j ACCEPT
iptables -A INPUT -p tcp --dport 7000 -j ACCEPT
iptables -A INPUT -p udp --dport 7000 -j ACCEPT

# Allow access to UI and API
#iptables -A INPUT -p tcp --dport 80 -m state --state NEW -j ACCEPT
#iptables -A INPUT -p tcp --dport 8000 -m state --state NEW -j ACCEPT

# Allow RTP packets
#iptables -A INPUT -p udp --dport 16384:32768 -j ACCEPT

# Accept inbound ICMP messages
iptables -A INPUT -p icmp --icmp-type echo-request -m limit --limit 1/sec -j ACCEPT

# Save rules
service iptables save

# List rules
iptables -L -nv
