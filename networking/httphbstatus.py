#!/usr/bin/env python

from subprocess import PIPE, Popen
from SocketServer import ThreadingMixIn
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler

port = 8888
command = "/etc/init.d/heartbeat"
command_arg = "status"

def s_cmd(self):
   s = Popen([command, command_arg], stdout=PIPE, stderr=PIPE)
   out, err = s.communicate()
   self.wfile.write(out)

class Handler(BaseHTTPRequestHandler):
   def do_GET(self):
       self.send_response(200)
       self.end_headers()
       s_cmd(self)
       return

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
   pass

if __name__ == '__main__':
   try:
       server = ThreadedHTTPServer(('', port), Handler)
       server.serve_forever()
   except KeyboardInterrupt:
       print '^C received, shutting down the web server'
       server.socket.close()

