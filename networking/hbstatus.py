#!/usr/bin/env python

from socket import *
from subprocess import *

host = ''
port = 8888

s = socket(AF_INET, SOCK_STREAM)    # create a TCP socket
s.bind((host, port))                # bind it to the server port
s.listen(5)                         # allow 5 simultaneous
                                    # pending connections


while 1:
    # wait for next client to connect
    connection, address = s.accept() # connection is a new socket
    output = Popen(["/etc/init.d/heartbeat", "status"], stdout=PIPE).communicate()[0]
    connection.send(output)
    connection.close()              # close socket
